package com.emapta.pmtool.domain.dto;

public class ProjectDTO extends BaseDTO {
    private int id;
    private String name;
    private String description;

    public ProjectDTO() {
    }

    public ProjectDTO(String name, String description, String crtBy) {
        super(crtBy);
        this.name = name;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
