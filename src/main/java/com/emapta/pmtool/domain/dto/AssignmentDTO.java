package com.emapta.pmtool.domain.dto;

public class AssignmentDTO extends BaseDTO {

    private String issueID;
    private String userID;

    public AssignmentDTO(String issueID, String userID, String crtBy) {
        this.issueID = issueID;
        this.userID = userID;
        this.setCrtBy(crtBy);
    }

    public String getIssueID() {
        return issueID;
    }

    public void setIssueID(String issueID) {
        this.issueID = issueID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
