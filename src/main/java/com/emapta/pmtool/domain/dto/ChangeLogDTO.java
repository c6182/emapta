package com.emapta.pmtool.domain.dto;

import java.sql.Date;

public class ChangeLogDTO {
    private Date changedOn;
    private int fromStateID;
    private String fromStateName;
    private int toStateID;
    private String toStateName;

    public Date getChangedOn() {
        return changedOn;
    }

    public void setChangedOn(Date changedOn) {
        this.changedOn = changedOn;
    }

    public int getFromStateID() {
        return fromStateID;
    }

    public void setFromStateID(int fromStateID) {
        this.fromStateID = fromStateID;
    }

    public String getFromStateName() {
        return fromStateName;
    }

    public void setFromStateName(String fromStateName) {
        this.fromStateName = fromStateName;
    }

    public int getToStateID() {
        return toStateID;
    }

    public void setToStateID(int toStateID) {
        this.toStateID = toStateID;
    }

    public String getToStateName() {
        return toStateName;
    }

    public void setToStateName(String toStateName) {
        this.toStateName = toStateName;
    }
}
