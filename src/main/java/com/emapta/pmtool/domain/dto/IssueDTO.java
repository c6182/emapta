package com.emapta.pmtool.domain.dto;

import java.util.LinkedList;
import java.util.List;

public class IssueDTO extends BaseDTO {
    private String id;
    private String title;
    private String desc;
    private int projectID;
    private String type;
    private String status;
    private int currentAssignee;
    private List<ChangeLogDTO> changeLogs;

    public IssueDTO() {
    }

    public IssueDTO(String title, String desc, int projectID, String type, String status, int currentAssignee, String crtBy) {
        super(crtBy);
        this.title = title;
        this.desc = desc;
        this.projectID = projectID;
        this.type = type;
        this.status = status;
        this.currentAssignee = currentAssignee;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getProjectID() {
        return projectID;
    }

    public void setProjectID(int projectID) {
        this.projectID = projectID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCurrentAssignee() {
        return currentAssignee;
    }

    public void setCurrentAssignee(int currentAssignee) {
        this.currentAssignee = currentAssignee;
    }

    public void addChangeLog(ChangeLogDTO changeLog) {
        if (this.changeLogs == null) {
            this.changeLogs = new LinkedList<>();
        }

        this.changeLogs.add(changeLog);
    }

    public List<ChangeLogDTO> getChangeLogs() {
        return changeLogs;
    }
}
