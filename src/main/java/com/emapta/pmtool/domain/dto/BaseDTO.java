package com.emapta.pmtool.domain.dto;

import java.sql.Date;

public class BaseDTO {
    private String crtBy;
    private Date crtOn;
    private String modBy;
    private Date modOn;

    public BaseDTO() {
    }

    public BaseDTO(String crtBy) {
        this.crtBy = crtBy;
    }

    public String getCrtBy() {
        return crtBy;
    }

    public void setCrtBy(String crtBy) {
        this.crtBy = crtBy;
    }

    public Date getCrtOn() {
        return crtOn;
    }

    public void setCrtOn(Date crtOn) {
        this.crtOn = crtOn;
    }

    public String getModBy() {
        return modBy;
    }

    public void setModBy(String modBy) {
        this.modBy = modBy;
    }

    public Date getModOn() {
        return modOn;
    }

    public void setModOn(Date modOn) {
        this.modOn = modOn;
    }
}
