package com.emapta.pmtool.domain.request;

public class IssueReq {
    private String title;
    private String desc;
    private int projectID;
    private String type;
    private String status;
    private int currentAssignee;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getProjectID() {
        return projectID;
    }

    public void setProjectID(int projectID) {
        this.projectID = projectID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCurrentAssignee() {
        return currentAssignee;
    }

    public void setCurrentAssignee(int currentAssignee) {
        this.currentAssignee = currentAssignee;
    }
}
