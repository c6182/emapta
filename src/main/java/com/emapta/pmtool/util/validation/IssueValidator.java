package com.emapta.pmtool.util.validation;

import com.emapta.pmtool.domain.request.IssueReq;
import com.emapta.pmtool.domain.request.ProjectReq;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class IssueValidator {

    private static final int DEFAULT_INT = 0;
    private static final int NAME_MAX_LEN = 100;
    private static final int DESC_MAX_LEN = 250;
    private static final int ID_MAX_LEN = 20;

    public List<String> validateProject(ProjectReq project) {

        List<String> errors = new LinkedList<>();
        Optional<ProjectReq> optProj = Optional.ofNullable(project);
        if (optProj.isPresent()) {
            if (project.getName() == null || project.getName().isEmpty()) {
                errors.add("Project name is not specified");
            } else {
                project.setName(project.getName().trim());
                if (project.getName().length() > NAME_MAX_LEN) {
                    errors.add(String.format("Project name is longer than allowed (%d)", NAME_MAX_LEN));
                }
            }

            if (project.getDesc() == null || project.getDesc().isEmpty()) {
                errors.add("A description is not supplied");
            } else {
                project.setDesc(project.getDesc().trim());
                if (project.getDesc().length() > NAME_MAX_LEN) {
                    errors.add(String.format("Project description is longer than allowed (%d)", DESC_MAX_LEN));
                }
            }
        } else {
            errors.add("No data supplied for the new project");
        }

        return errors;
    }

    public List<String> validateIssue(IssueReq request) {
        List<String> errors = new LinkedList<>();
        Optional<IssueReq> optIssue = Optional.ofNullable(request);
        if (optIssue.isPresent()) {
            if (request.getTitle() == null || request.getTitle().isEmpty()) {
                errors.add("Issue title is not specified");
            } else {
                request.setTitle(request.getTitle().trim());
                if (request.getTitle().length() > NAME_MAX_LEN) {
                    errors.add(String.format("Issue title is longer than allowed (%d)", NAME_MAX_LEN));
                }
            }

            if (request.getDesc() == null || request.getDesc().isEmpty()) {
                errors.add("A description is not supplied for the issue");
            } else {
                request.setDesc(request.getDesc().trim());
                if (request.getDesc().length() > NAME_MAX_LEN) {
                    errors.add(String.format("Issue description is longer than allowed (%d)", NAME_MAX_LEN));
                }
            }

            if (request.getProjectID() == DEFAULT_INT) {
                errors.add("The project to which the issue belongs is not specified");
            }

            if (request.getType() == null || request.getType().isEmpty()) {
                errors.add("Issue type is not specified");
            } else {
                request.setType(request.getType().trim());
                if (request.getType().length() > ID_MAX_LEN) {
                    errors.add(String.format("Issue type code is longer than allowed (%d)", ID_MAX_LEN));
                }
            }

            if (request.getStatus() == null || request.getStatus().isEmpty()) {
                errors.add("Issue status is not specified");
            } else {
                request.setStatus(request.getStatus().trim());
                if (request.getStatus().length() > ID_MAX_LEN) {
                    errors.add(String.format("Issue status code is longer than allowed (%d)", ID_MAX_LEN));
                }
            }

            if (request.getCurrentAssignee() == DEFAULT_INT) {
                errors.add("Current assignee is not specified");
            }
        } else {
            errors.add("No data supplied for the new issue");
        }

        return errors;
    }
}
