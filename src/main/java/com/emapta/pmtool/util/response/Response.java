package com.emapta.pmtool.util.response;

import org.springframework.http.ResponseEntity;

public interface Response {

    ResponseEntity<ResponseData> create(int statusCode);

    ResponseEntity<ResponseData> create(int statusCode, String message);

    ResponseEntity<ResponseData> create(int statusCode, String message, Object data);

    ResponseEntity<ResponseData> create(int statusCode, Object data);
}
