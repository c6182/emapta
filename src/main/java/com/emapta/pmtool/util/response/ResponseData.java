package com.emapta.pmtool.util.response;

import org.springframework.http.ResponseEntity;

public class ResponseData<T> {

    private final int statusCode;
    private final String message;
    private final T data;

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }

    public ResponseData(ResponseDataBuilder<T> builder) {
        this.statusCode = builder.statusCode;
        this.message = builder.message;
        this.data = builder.data;
    }

    public static class ResponseDataBuilder<T> {
        private final int statusCode;
        private String message;
        private T data;

        public ResponseDataBuilder(int statusCode) {
            this.statusCode = statusCode;
        }

        public ResponseDataBuilder<T> message(String message) {
            this.message = message;
            return this;
        }

        public ResponseDataBuilder<T> data(T data) {
            this.data = data;
            return this;
        }

        public ResponseEntity<ResponseData> build() {
            ResponseData<T> responseData = new ResponseData<>(this);
            return ResponseEntity.status(responseData.getStatusCode()).body(responseData);
        }
    }
}
