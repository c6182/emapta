package com.emapta.pmtool.util.response;

import org.springframework.http.ResponseEntity;

public class ResponseImpl implements Response {

    @Override
    public ResponseEntity<ResponseData> create(int statusCode) {
        return new ResponseData.ResponseDataBuilder<>(statusCode).build();
    }

    @Override
    public ResponseEntity<ResponseData> create(int statusCode, String message) {
        return new ResponseData.ResponseDataBuilder<>(statusCode).message(message).build();
    }

    @Override
    public ResponseEntity<ResponseData> create(int statusCode, String message, Object data) {
        return new ResponseData.ResponseDataBuilder<>(statusCode).message(message).data(data).build();
    }

    @Override
    public ResponseEntity<ResponseData> create(int statusCode, Object data) {
        return new ResponseData.ResponseDataBuilder<>(statusCode).data(data).build();
    }
}
