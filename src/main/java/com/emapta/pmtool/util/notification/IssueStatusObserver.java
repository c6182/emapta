package com.emapta.pmtool.util.notification;

/**
 * Represents a single user interested in state-change of issues
 */
public class IssueStatusObserver extends StatusObserver {

    private String userID;
    private String userName;
    private String userEmail;
    private String issueID;

    public IssueStatusObserver(IssueStatusSubject subject, String userID, String userName, String userEmail, String issueID) {
        this.userID = userID;
        this.userName = userName;
        this.userEmail = userEmail;
        this.issueID = issueID;

        this.subject = subject;
        this.subject.attach(issueID,this);
    }

    public void update() {
        String updatedStatus = subject.getIssueStatus(this.issueID);
        String userEmail = this.getUserEmail();

        // TODO Dispatch email to userEmail with "updatedStatus"

        // TODO Push notification using userID with "updatedStatus"

    }

    public String getUserID() {
        return userID;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public String getIssueID() {
        return issueID;
    }
}
