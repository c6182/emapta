package com.emapta.pmtool.util.notification;

public abstract class StatusObserver {
    protected IssueStatusSubject subject;
    public abstract void update();
}
