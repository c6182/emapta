package com.emapta.pmtool.util.notification;

import java.util.*;

public class IssueStatusSubject {
    Map<String, List<IssueStatusObserver>> observers = new HashMap<>();
    Map<String, String> issueStatuses = new HashMap<>();
    private static volatile IssueStatusSubject instance;

    private IssueStatusSubject() {}

    public static IssueStatusSubject getInstance() {
        if (instance == null) {
            synchronized (IssueStatusSubject.class) {
                instance = new IssueStatusSubject();
            }
        }
        return instance;
    }

    public void attach(String issueID, IssueStatusObserver observer) {
        List<IssueStatusObserver> obsList = observers.get(issueID);
        if (obsList == null) {
            obsList = new LinkedList<>();
            observers.put(issueID, obsList);
        }
        obsList.add(observer);
    }

    public void setState(String issueID, String updatedStatus) {
        this.issueStatuses.put(issueID, updatedStatus);
        notifyObservers(issueID);
    }

    public void notifyObservers(String issueID) {
        observers.get(issueID).forEach(IssueStatusObserver::update);
    }

    public String getIssueStatus(String issueID) {
        return this.issueStatuses.get(issueID);
    }

}
