package com.emapta.pmtool.util.exception;

public class PMToolException extends RuntimeException {

    public PMToolException(Throwable exception) {
        super(exception);
    }

    public PMToolException(String message, Throwable exception) {
        super(message, exception);
    }
}
