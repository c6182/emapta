package com.emapta.pmtool.util.exception;

public class DataAccessException extends PMToolException {

    public DataAccessException(Throwable exception) {
        super(exception);
    }

    public DataAccessException(String message, Throwable exception) {
        super(message, exception);
    }
}
