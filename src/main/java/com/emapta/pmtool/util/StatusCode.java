package com.emapta.pmtool.util;

public enum StatusCode {
    SUCCESS(0, "Success"),
    ERROR(1, "Error"),
    FAILED(2, "Failed"),
    INVALID_REQUEST(3, "Invalid request"),
    NOT_FOUND(4, "Nothing found");

    int code;
    String name;

    StatusCode(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int code() {
        return code;
    }

    public String description() {
        return name;
    }
}
