package com.emapta.pmtool.repository.impl;

import com.emapta.pmtool.domain.dto.AssignmentDTO;
import com.emapta.pmtool.domain.dto.IssueDTO;
import com.emapta.pmtool.domain.dto.ProjectDTO;

import java.util.List;

public interface IssueDao {

    int addProject(ProjectDTO project);

    int addIssue(IssueDTO issue);

    List<IssueDTO> getIssuesOfProject(int projectID);

    List<ProjectDTO> getProjects();

    int assign(AssignmentDTO assignment);
}
