package com.emapta.pmtool.repository;

import com.emapta.pmtool.domain.dto.AssignmentDTO;
import com.emapta.pmtool.domain.dto.ChangeLogDTO;
import com.emapta.pmtool.domain.dto.IssueDTO;
import com.emapta.pmtool.domain.dto.ProjectDTO;
import com.emapta.pmtool.repository.impl.IssueDao;
import com.emapta.pmtool.util.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.*;


public class IssueDaoImpl implements IssueDao {
    private static final Logger logger = LoggerFactory.getLogger(IssueDaoImpl.class);

    @Autowired
    private NamedParameterJdbcTemplate namedJdbcTemplate;

    @Override
    public int addProject(ProjectDTO project) {
        String query = "INSERT INTO PROJECT (NAME, DESC, CRTON, CRTBY) " +
                "VALUES (:name, :desc, GETDATE(), :crtBy)";
        try {
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put(":name", project.getName());
            paramMap.put(":desc", project.getDescription());
            paramMap.put(":crtBy", project.getCrtBy());
            return namedJdbcTemplate.update(query, paramMap);

        } catch (Exception e) {
            logger.error("addProject(): error occurred while accessing DB", e);
            throw new DataAccessException("Data access error", e);
        }
    }

    @Override
    public int addIssue(IssueDTO issue) {
        String query = "INSERT INTO PROJECT (TITLE, DESC, PR_ID, TYPE, STATE, CRNT_ASSIGNEE, CRTON, CRTBY) " +
                "VALUES (:title :desc, :prID, :type, :state, :crntAssignee, GETDATE(), :crtBy)";
        try {
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put(":title", issue.getTitle());
            paramMap.put(":desc", issue.getDesc());
            paramMap.put(":prID", issue.getProjectID());
            paramMap.put(":type", issue.getType());
            paramMap.put(":state", issue.getStatus());
            paramMap.put(":crntAssignee", issue.getCurrentAssignee());
            paramMap.put(":crtBy", issue.getCrtBy());
            return namedJdbcTemplate.update(query, paramMap);

        } catch (Exception e) {
            logger.error("addIssue(): error occurred while accessing DB", e);
            throw new DataAccessException("Data access error", e);
        }
    }

    @Override
    public List<ProjectDTO> getProjects() {

        String query = "SELECT ID, NAME, DESC, CRTON, CRTBY, MODON, MODBY FROM PROJECT;";

        try {
            return namedJdbcTemplate.query(query, (rs, i) -> {
                ProjectDTO project = new ProjectDTO();
                project.setId(rs.getInt("ID"));
                project.setName((rs.getString("NAME")));
                project.setDescription((rs.getString("DESC")));
                project.setCrtOn((rs.getDate("CRTON")));
                project.setCrtBy((rs.getString("CRTBY")));
                project.setModBy((rs.getString("MODBY")));
                project.setModOn(rs.getDate("MODON"));
                return project;
            });
        } catch (Exception e) {
            logger.error("getProjects(): error occurred while accessing DB", e);
            throw new DataAccessException(e);
        }
    }

    @Override
    public List<IssueDTO> getIssuesOfProject(int projectID) {
        Map<String, IssueDTO> issueMap = new HashMap<>();

        String query = "SELECT I.ID, IT.NAME AS TYPENAME, S.NAME AS STATUSNAME, CL.CRTON AS CHANGED_ON, CL.FROM_STATE_NAME, CL.TO_STATE_NAME " +
                "FROM ISSUE I " +
                "JOIN ISSUETYPE IT ON (I.TYPE = IT.ID) " +
                "JOIN ISSUESTATE S ON (I.STATE = S.ID) " +
                "LEFT JOIN CHANGELOG CL ON (CL.IS_ID = I.ID) " +
                "WHERE (I.PR_ID = :prID) " +
                "ORDER BY CL.CRTON DESC;";

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("prID", projectID);

        try {
            namedJdbcTemplate.query(query, paramMap, (rs, i) -> {
                IssueDTO issue = null;
                String issueID = rs.getString("ID");

                if (!issueMap.containsKey(issueID)) { // This is done becuase Issue:ChangeLogDTO = 1:M
                    issue = new IssueDTO();

                    issue.setId(issueID);
                    issue.setType((rs.getString("TYPENAME")));
                    issue.setStatus((rs.getString("STATUSNAME")));
                } else {
                    issue = issueMap.get(issueID);
                }

                ChangeLogDTO changeLog = new ChangeLogDTO();
                changeLog.setChangedOn((rs.getDate("CHANGED_ON")));
                changeLog.setFromStateName((rs.getString("FROM_STATE_NAME")));
                changeLog.setToStateName((rs.getString("TO_STATE_NAME")));
                issue.addChangeLog(changeLog);

                return issue;
            });
        } catch (Exception e) {
            logger.error("getIssuesOfProject(): error occurred while accessing DB", e);
            throw new DataAccessException(e);
        }

        return new LinkedList<>(issueMap.values()); // Return all map values (i.e. unique issues)
    }

    @Override
    public int assign(AssignmentDTO assignment) {
        String query = "INSERT INTO INTERACTION (IS_ID, ASSIGNEE, CRTON, CRTBY) " +
                "VALUES (:issueID :userID, GETDATE(), :crtBy)";
        try {
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put(":issueID", assignment.getIssueID());
            paramMap.put(":userID", assignment.getUserID());
            paramMap.put(":crtBy", assignment.getCrtBy());
            return namedJdbcTemplate.update(query, paramMap);

        } catch (Exception e) {
            logger.error("assign(): error occurred while accessing DB", e);
            throw new DataAccessException("Data access error", e);
        }
    }
}
