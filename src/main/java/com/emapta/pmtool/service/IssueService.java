package com.emapta.pmtool.service;

import com.emapta.pmtool.domain.request.IssueReq;
import com.emapta.pmtool.domain.request.ProjectReq;
import com.emapta.pmtool.util.response.ResponseData;
import org.springframework.http.ResponseEntity;

public interface IssueService {

    ResponseEntity<ResponseData> addProject(ProjectReq request);

    ResponseEntity<ResponseData> addIssue(IssueReq request);

    ResponseEntity<ResponseData> getIssuesOfProject(int projectID);

    ResponseEntity<ResponseData> getProjects();

    ResponseEntity<ResponseData> assign(String issueID, String assigneeID);
}
