package com.emapta.pmtool.service.impl;

import com.emapta.pmtool.domain.dto.AssignmentDTO;
import com.emapta.pmtool.domain.dto.IssueDTO;
import com.emapta.pmtool.domain.dto.ProjectDTO;
import com.emapta.pmtool.domain.request.IssueReq;
import com.emapta.pmtool.domain.request.ProjectReq;
import com.emapta.pmtool.repository.impl.IssueDao;
import com.emapta.pmtool.service.IssueService;
import com.emapta.pmtool.util.StatusCode;
import com.emapta.pmtool.util.exception.PMToolException;
import com.emapta.pmtool.util.notification.IssueStatusObserver;
import com.emapta.pmtool.util.notification.IssueStatusSubject;
import com.emapta.pmtool.util.response.Response;
import com.emapta.pmtool.util.response.ResponseData;
import com.emapta.pmtool.util.validation.IssueValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class IssueServiceImpl implements IssueService {
    private static final Logger logger = LoggerFactory.getLogger(IssueServiceImpl.class);

    @Autowired
    Response response;

    @Autowired
    IssueValidator issueValidator;

    @Autowired
    IssueDao issueDao;

    @Override
    public ResponseEntity<ResponseData> addProject(ProjectReq request) {
        // TODO Authenticate user
        String user = ""; // TODO Get from authentication result

        try {
            // Validate params
            List<String> errors = issueValidator.validateProject(request);

            if (errors.isEmpty()) {
                // Transform into BaseDTO
                ProjectDTO project = new ProjectDTO(request.getName(), request.getDesc(), user);

                // Persist data
                int result = issueDao.addProject(project);

                // Return response based on result
                if (result < 1) {
                    return response.create(StatusCode.FAILED.code(), "Project was not added.");
                } else {
                    return response.create(StatusCode.SUCCESS.code(), StatusCode.SUCCESS.description());
                }

            } else {
                return response.create(StatusCode.ERROR.code(), StatusCode.ERROR.description(), Collections.singletonMap("Errors", errors));
            }
        } catch (Exception ex) {
            String errorTxt = "Exception occurred while trying to persist data";
            logger.error(String.format("addProject(): %s", errorTxt), ex);
            throw new PMToolException(errorTxt, ex);
        }
    }

    @Override
    public ResponseEntity<ResponseData> addIssue(IssueReq request) {
        // TODO Authenticate user
        String user = ""; // TODO Get from authentication result

        try {
            // Validate params
            List<String> errors = issueValidator.validateIssue(request);

            if (errors.isEmpty()) {
                // Transform into BaseDTO
                IssueDTO issue = new IssueDTO(request.getTitle(), request.getDesc(), request.getProjectID(),
                        request.getType(), request.getStatus(), request.getCurrentAssignee(), user);

                // Persist data
                int result = issueDao.addIssue(issue);

                // Return response based on result
                if (result < 1) {
                    return response.create(StatusCode.FAILED.code(), "Issue was not added.");
                } else {
                    return response.create(StatusCode.SUCCESS.code(), StatusCode.SUCCESS.description());
                }

            } else {
                return response.create(StatusCode.ERROR.code(), StatusCode.ERROR.description(), Collections.singletonMap("Errors", errors));
            }
        } catch (Exception ex) {
            String errorTxt = "Exception occurred while trying to persist data";
            logger.error(String.format("addIssue(): %s", errorTxt), ex);
            throw new PMToolException(errorTxt, ex);
        }
    }

    @Override
    public ResponseEntity<ResponseData> getProjects() {
        // TODO Authenticate user

        try {
            List<ProjectDTO> projects = issueDao.getProjects();
            if (projects.isEmpty()) {
                return response.create(StatusCode.NOT_FOUND.code(), "No data found");
            } else {
                return response.create(StatusCode.SUCCESS.code(), projects);
            }
        } catch (Exception ex) {
            String errorTxt = "Exception occurred while trying to fetch data";
            logger.error(String.format("getProjects(): %s", errorTxt), ex);
            throw new PMToolException(errorTxt, ex);
        }
    }

    @Override
    public ResponseEntity<ResponseData> getIssuesOfProject(int projectID) {
        // TODO Authenticate user

        List<String> errors = new LinkedList<>();

        try {
            if (projectID < 1) {
                errors.add("Invalid project ID supplied");
            }

            if (errors.isEmpty()) {
                List<IssueDTO> issues = issueDao.getIssuesOfProject(projectID);
                if (issues.isEmpty()) {
                    return response.create(StatusCode.NOT_FOUND.code(), "No data found for the supplied arguments");
                } else {
                    return response.create(StatusCode.SUCCESS.code(), issues);
                }
            } else {
                return response.create(StatusCode.INVALID_REQUEST.code(), "Invalid arguments supplied.");
            }
        } catch (Exception ex) {
            String errorTxt = "Exception occurred while trying to fetch data";
            logger.error(String.format("getIssuesOfProject(): %s", errorTxt), ex);
            throw new PMToolException(errorTxt, ex);
        }
    }

    @Override
    public ResponseEntity<ResponseData> assign(String issueID, String assigneeID) {
        // TODO Authenticate user
        String user = ""; // TODO Get from authentication result

        try {
            // TODO Validate supplied issueID, validate supplied userID
            List<String> errors = new LinkedList<>(); // issueValidator.validateAssignRequest(...);

            // If OK, proceed

            if (errors.isEmpty()) {
                // Create BaseDTO
                AssignmentDTO assignment = new AssignmentDTO(issueID, assigneeID, user);

                // Persist data
                int result = issueDao.assign(assignment);

                // Return response based on result
                if (result < 1) {
                    return response.create(StatusCode.FAILED.code(), "Issue assignment failed.");

                } else {
                    // TODO Get user data for assignee // userDao.getUser(assigneeID);
                    String assigneeUserName = "";
                    String assigneeEmail = "";

                    // The "user" just assigned the issue to the other user represented by this "assigneeID"; thus the assignee just became a stakeholder
                    // Create an observer for this assignee and attach to subject, to be notified of changes to this particular issueID
                    new IssueStatusObserver(IssueStatusSubject.getInstance(), assigneeID, assigneeUserName, assigneeEmail, issueID);

                    return response.create(StatusCode.SUCCESS.code(), StatusCode.SUCCESS.description());
                }

            } else {
                return response.create(StatusCode.ERROR.code(), StatusCode.ERROR.description(), Collections.singletonMap("Errors", errors));
            }
        } catch (Exception ex) {
            String errorTxt = "Exception occurred while trying to persist data";
            logger.error(String.format("assign(): %s", errorTxt), ex);
            throw new PMToolException(errorTxt, ex);
        }
    }
}
