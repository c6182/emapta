package com.emapta.pmtool.controller;

import com.emapta.pmtool.domain.request.IssueReq;
import com.emapta.pmtool.domain.request.ProjectReq;
import com.emapta.pmtool.service.IssueService;
import com.emapta.pmtool.util.response.ResponseData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/issue")
public class IssueController {

    @Autowired
    IssueService issueService;

    @PostMapping("/add-project")
    public ResponseEntity<ResponseData> addProject(@RequestBody ProjectReq request) {
        return issueService.addProject(request);
    }

    @PostMapping("/add-issue")
    public ResponseEntity<ResponseData> addIssue(@RequestBody IssueReq request) {
        return issueService.addIssue(request);
    }

    @GetMapping("/projects")
    public ResponseEntity<ResponseData> getIssuesOfProject() {
        return issueService.getProjects();
    }

    @GetMapping("/issues")
    public ResponseEntity<ResponseData> getIssuesOfProject(@RequestParam int projectID) {
        return issueService.getIssuesOfProject(projectID);
    }

    @PutMapping("/assign-issue")
    public ResponseEntity<ResponseData> assign(@RequestParam String issueID, @RequestParam String assigneeID) {
        return issueService.assign(issueID, assigneeID);
    }
}
